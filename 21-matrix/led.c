
/*注意，本程序执行需要把拨码开关打到OFF状态，P15全部断开，P12纵向连接*/

#include <AT89X52.H>	//包含头文件
#define led P0
sbit line1=P2^0;
sbit line2=P2^1;
sbit s4=P0^0;
sbit s5=P0^1;
sbit s6=P0^2;
sbit s7=P0^3;
sbit s8=P0^4;
sbit s9=P0^5;
sbit s10=P0^6;
sbit s11=P0^7;


void main(void)
{
	int i;
	line1=0;//两行全部拉低，准备检测
	line2=0;
	for(;;)
	{
		if((P2 & 0xf0)!=0xf0)//检测是否有键按下，有的话进入条件
		{
			i=10000;
			while(i--);//去抖动延时
			line1=0;
			line2=1;//检测下面一行是否按下
			if((P2 & 0xf0)!=0xf0)//有按下则区别哪个键被按下
			{
				if(P2_4==0)//S4按下
				{
					P0=0XFF;//灯全灭
					s4=0;//对应灯亮
				}
				if(P2_5==0)//S5
				{
					P0=0XFF;
					s5=0;
				}
				if(P2_6==0)//S6
				{
					P0=0XFF;
					s6=0;
				}
				if(P2_7==0)//S7
				{
					P0=0XFF;
					s7=0;
				}
			}
			else
			{
				line1=1;
				line2=0;
				if((P2 & 0xf0)!=0xf0)//检测上面一行是否被按下
				{
					if(P2_4==0)//S8
					{
						P0=0XFF;
						s8=0;
					}
					if(P2_5==0)//S9
					{
						P0=0XFF;
						s9=0;
					}
					if(P2_6==0)//S10
					{
						P0=0XFF;
						s10=0;
					}
					if(P2_7==0)//S11
					{
						P0=0XFF;
						s11=0;
					}
				}

			
			}
			line1=0;
			line2=0;//行线归0，开始新一轮检测

		}
		else
		{
			P0=0XFF;//没有键按下则全部灯灭
			line1=line2=0;//行线归0，开始新一轮检测
		}
	} 
}
