#ifdef SDCC
#include <at89x52.h>
#else
#include <AT89X52.H>	//包含头文件
#endif


#define led P0

void Delay(unsigned int i)
{
	while(i--);
}

void main(void)
{
	unsigned char d;
	led=0xff;
	for(;;)
	{
		d=0X01;	//初始值
		while(d!=0)	//实现8次左移，如果d中唯一的位1移动出范围，则重新赋值开始
		{
			led=~d;	//反向输出点亮
			Delay(60000);
			d<<=1;	//左移动一位
			Delay(60000);
		}

	} 
}
