#ifdef SDCC
#include <8051.h>
//#include "intrins.h"
#else
#include <reg51.H>
#include "intrins.h"
#endif

#define uint unsigned int
#define uchar unsigned char

//ADC0832的引脚
#ifdef SDCC
sbit at (0xB2) ADCS;
sbit at (0xB4) ADDI;
sbit at (0xB4) ADDO;
sbit at (0xB5) ADCLK;

#else
sbit ADCS =P3^2;  //ADC0832 chip seclect
sbit ADDI =P3^4;  //ADC0832 k in
sbit ADDO =P3^4;  //ADC0832 k out
sbit ADCLK =P3^5;  //ADC0832 clock signal
#endif

unsigned char dispbitcode[8]={0xf7,0xfb,0xfd,/*0xfe,0xef,0xdf,0xbf,0x7f*/};  //位扫描
unsigned char dispcode[11]={0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,0xF8,0x80,0x90,0xff};  //共阳数码管字段码
unsigned char dispbuf[3];
uint temp;
uchar getdata; //获取ADC转换回来的值
/*=====0-9=====A-G=====*/
unsigned char a[16]={0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,
             0x80,0x90,0x88,0x83,0xc6,0xa1,0x86,0x8e};
			 //共阳极数码管的段码0 1 2 3 4 5 6 7 8 9 A B C D E F

#define shuma P0
#ifdef SDCC
sbit at (0x90) LED_0;
sbit at (0x91) LED_1;
sbit at (0x92) LED_2;
sbit at (0x93) LED_3;
sbit at (0x94) LED_4;
sbit at (0x95) LED_5;
sbit at (0x96) LED_6;
sbit at (0x97) LED_7;
#else
sbit LED_0=P1^4;
sbit LED_1=P1^5;
sbit LED_2=P1^6;
sbit LED_3=P1^7;
sbit LED_4=P1^0;
sbit LED_5=P1^1;
sbit LED_6=P1^2;
sbit LED_7=P1^3;
#endif



void delay(unsigned int x);
void display1(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4);
void delay(unsigned int x)
{
  	unsigned int i;
  	for(i=0;i<x;i++);
}
void display1(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4)
{
  	shuma=a[d1];	//选中第一位，发送第一位段码
  	LED_0=0;
  	delay(100);
  	LED_0=1;

  	shuma=a[d2];//选中第二位，发送第二位段码
  	LED_1=0;
  	delay(100);
  	LED_1=1;

  	shuma=a[d3];//选中第三位，发送第三位段码
  	LED_2=0;
  	delay(100);
  	LED_2=1;

  	shuma=a[d4];//选中第四位，发送第四位段码
  	LED_3=0;
  	delay(100);
  	LED_3=1; 
}

#ifdef SDCC
void _nop_()
{
	__asm
		nop
	__endasm;
}
#endif

/************
读ADC0832函数
************/

//采集并返回
unsigned int Adc0832(unsigned char channel)
{
 	uchar i=0;
	uchar j;
	uint dat=0;
	uchar ndat=0;

	if(channel==0)channel=2;
	if(channel==1)channel=3;
	ADDI=1;
	_nop_();
	_nop_();
	ADCS=0;//拉低CS端
	_nop_();
	_nop_();
	ADCLK=1;//拉高CLK端
	_nop_();
	_nop_();
	ADCLK=0;//拉低CLK端,形成下降沿1
	_nop_();
	_nop_();
	ADCLK=1;//拉高CLK端
	ADDI=channel&0x1;
	_nop_();
	_nop_();
	ADCLK=0;//拉低CLK端,形成下降沿2
	_nop_();
	_nop_();
	ADCLK=1;//拉高CLK端
	ADDI=(channel>>1)&0x1;
	_nop_();
	_nop_();
	ADCLK=0;//拉低CLK端,形成下降沿3
	ADDI=1;//控制命令结束 
	_nop_();
	_nop_();
	dat=0;
	for(i=0;i<8;i++)
	{
		dat|=ADDO;//收数据
		ADCLK=1;
		_nop_();
		_nop_();
		ADCLK=0;//形成一次时钟脉冲
		_nop_();
		_nop_();
		dat<<=1;
		if(i==7)dat|=ADDO;
	}  
	for(i=0;i<8;i++)
	{
		j=0;
		j=j|ADDO;//收数据
		ADCLK=1;
		_nop_();
		_nop_();
		ADCLK=0;//形成一次时钟脉冲
		_nop_();
		_nop_();
		j=j<<7;
		ndat=ndat|j;
		if(i<7)ndat>>=1;
	}
	ADCS=1;//拉低CS端
	ADCLK=0;//拉低CLK端
	ADDO=1;//拉高数据端,回到初始状态
	dat<<=8;
	dat|=ndat;
 	return(dat);            //return ad data
}




void main(void)
{ 


while(1)
  {
  getdata=Adc0832(0);
  temp=getdata*1.0/255*500;  //电压值转换，5V做为参考电压，分成256份。
  dispbuf[0]=temp%10;  //个位
  dispbuf[2]=temp/10%10; //十位
  dispbuf[1]=temp/100; //百位
    display1(0,getdata/100%10,getdata/10%10,getdata%10);  
  }
} 
