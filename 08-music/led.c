#include <AT89X52.H>	//包含头文件
sbit buz=P3^2;
sbit led=P0^0;



void Music(unsigned char number);
void delay10ms(unsigned char time);
void delay50us(unsigned char time);
unsigned char code SOUNDLONG[];
unsigned char code SOUNDTONE[];

void main()
{
	while(1)
	{
		Music(1);  //生日快乐
		delay10ms(200);
		delay10ms(200);
		Music(2);  //三轮车
		delay10ms(200);
		delay10ms(200);
	}
}

void Music(unsigned char number)
{
	unsigned int k,n;
	unsigned int SoundLong,SoundTone;
	unsigned int i,j,m;

	for(k=0;k<number-1;k++)
	{
		while(SOUNDLONG[i] !=0){i++;}
		i++;
		if(i>=57) i=0;
	}
	for(k=0;k<number-1;k++)
	{
		while(SOUNDTONE[j] !=0){j++;}
		j++;
		if(j>=57) j=0;
	}

	do
	{
		if(i>=57) i=0;
		if(j>=57) j=0;
		SoundLong=SOUNDLONG[i];
		SoundTone=SOUNDTONE[j];
		i++;
		j++;

		for(n=0;n<SoundLong;n++)
		{
			for(k=0;k<12;k++)
			{
				buz=0;
				for(m=0;m<SoundTone/2;m++)
					;
				buz=1;
				for(m=0;m<SoundTone/2;m++)
					;
			}
		}
		delay50us(11);
	}while((SOUNDLONG[i] !=0) || (SOUNDTONE[j] !=0));
}

//延时程序
void delay10ms(unsigned char time)
{
	unsigned char a,b,c;
	for(a=0;a<time;a++)
		for(b=0;b<10;b++)
			for(c=0;c<120;c++)
				;
}
void delay50us(unsigned char time)
{
	unsigned char a,b;
	for(a=0;a<time;a++)
		for(b=0;b<6;b++)
			;
}

unsigned char code SOUNDLONG[]=
{   9,3,12,12,12,24,
	9,3,12,12,12,24,
	9,3,12,12,12,12,12,
	9,3,12,12,12,24,
	0, //生日快乐end
	9,3,12,12,12,24,
	9,3,12,12,12,24,
	9,3,12,12,12,12,12,
	9,3,12,12,12,24,
	0, //生日快乐end
};

unsigned char code SOUNDTONE[]=
{
	212,212,190,212,159,169,
	212,212,190,212,142,159,
	212,212,106,126,159,169,190,
	119,119,126,159,142,159,
	0, //生日快乐end
	212,212,190,212,159,169,
	212,212,190,212,142,159,
	212,212,106,126,159,169,190,
	119,119,126,159,142,159,
	0, //生日快乐end
};
