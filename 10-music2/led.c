#include <AT89X52.H>	//包含头文件
sbit buz=P3^2;
sbit led=P2^0;
//11.0592振荡器，实现50ms定时，计数单位为11.0592/12=0.9216，取倒数则为1.085微妙，则50ms有对应节拍46082，所以计数初值为65535-46082=4BFF

unsigned char m,n;
unsigned int code ton[7];


void int1() interrupt 3 //T1中断，时间是50ms
{
	TR1=0; //关中断
	TH1=(ton[n]&0xff00)>>8; //装定时器初值
	TL1=ton[n]&0x00ff;
	P2=n;
	ET1=1; //开中断
	TR1=1;
	m++;
	buz=~buz;
	if(m>=200)
	{
		m=0;
		n++;
		if(n>=6)
			n=0;

	} //中断20次，使时间为一秒，设置标志位
}

void main( void )
{
	n=0;
	m=0;
	TMOD=0x10; //设置为T1定时器
	TH1=0xf1; //装定时器初值
	TL1=0x16;
	TR1=1; //开中断
	ET1=1;
	EA=1;
	while(1)
	{
		if(n) //检测标志位
		{
			//n=0; //清除标志位
			//TH1=0xf3; //装定时器初值
			//TL1=0xc0;
			//buz=~buz; //蜂鸣器控制端状态间断相反，使蜂鸣器发出声音
		}
	}
}
unsigned int code ton[7]={0xf8b3,0xf972,0xfa14,0xfa66,0xfb03,0xfb8f,0xfc0b};
/*
c1:262Hz--thtl=0xf8b3
d1:294Hz--0xf972
e1:330Hz--0xfa14
f1:349Hz--0xfa66
g1:392Hz--0xfb03
a1:440Hz--0xfb8f
b1:494Hz--0xfc0b
如果把所有的半音的频率都列出来，是等比数列，其中国际标准音A的频率为440Hz。
*/
