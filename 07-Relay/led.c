#include <AT89X52.H>	//包含头文件
sbit relay=P3^2;
sbit buz=P3^3;
sbit led=P0^0;
void Delay(unsigned int i)
{
	while(i--);
}
void main(void)
{
	for(;;)
	{

		buz=0;		//1不响
		relay=0;		//2响
		led=1;		//LED灭
		Delay(60000);	//延时
		relay=1;		//1响
		buz=1;		//2不响
		led=0;		//LED亮
		Delay(60000);	//延时

	} 
}
