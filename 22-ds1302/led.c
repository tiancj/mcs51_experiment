#include <AT89X52.H>	//包含头文件
//#include<reg51.h>


/*************************ds1302与at89s52引脚连接********************/
sbit T_RST=P3^7; 
sbit T_CLK=P3^5;                 
sbit T_IO=P3^4;                 
sbit LED_0=P1^4;
sbit LED_1=P1^5;
sbit LED_2=P1^6;
sbit LED_3=P1^7;
sbit LED_4=P1^0;
sbit LED_5=P1^1;
sbit LED_6=P1^2;
sbit LED_7=P1^3;
sbit ACC0=ACC^0;
sbit ACC7=ACC^7;
unsigned char seg[]={0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,0x80,0x90};         //0~~9段码

void delay(unsigned char a);

/******************DS1302：写入操作(上升沿)*********************/ 
void write_byte(unsigned char da)
{
   unsigned char i;
   ACC=da;
   for(i=8;i>0;i--)
   { 
      T_IO=ACC0;
	  T_CLK=0;     
      T_CLK=1;
      ACC=ACC>>1;
   }
}

/******************DS1302：读取操作（下降沿）*****************/
unsigned char read_byte(void)
{
   unsigned char i;
   for(i=0;i<8;i++)
   {
      ACC=ACC>>1;
	  T_CLK = 1;
	  T_CLK = 0;
	  //delay(2);
      ACC7 = T_IO;
   }
   return(ACC);

}

/******************DS1302:写入数据（先送地址，再写数据）***************************/ 
void write_1302(unsigned char addr,unsigned char da)
{
   T_RST=0;    //停止工作
   T_CLK=0;                                 
   T_RST=1;   //重新工作
   write_byte(addr);    //写入地址
   
   write_byte(da);
   T_RST=0;
   T_CLK=1;
}

/******************DS1302:读取数据（先送地址，再读数据）**************************/
unsigned char read_1302(unsigned char addr)
{
   unsigned char temp;
   T_RST=0;                                   //停止工作
   T_CLK=0;  
   T_RST=1;                         //重新工作
   write_byte(addr);   //写入地址
   temp=read_byte();
   T_RST=0;
   T_CLK=1;     //停止工作
   return(temp);
}





/***********************延时程序=a*1ms**************************************/
void delay(unsigned char a)
{
   unsigned char i;
   while(a-- !=0)
   {
        for(i=0;i<80;i++);
   }
}


/***********************显示程序**********************************************/
/* 动态扫描条件（单个LED在1秒内）：  count >=50次   //点亮次数               */
/*                                   time  >=2ms    //持续时间               */
/* DS1302秒，分，时寄存器是BCD码形式：  用16求商和余进行"高4位"和"低4位"分离 */
/*                              */
/****************************************************************************/           
void led_disp(unsigned char *poi)
{  
   /**/P0=seg[*poi % 16];                    //第1个数码管：显示秒（个位）;
  	LED_5=0;
  	delay(3);
  	LED_5=1;


   P0=seg[*poi /16];               //第2个数码管：显示秒（十位）；
  	LED_4=0;
  	delay(3);
  	LED_4=1;

   P0=seg[*(poi+1) % 16];            //第4个数码管：显示分（个位）
  	LED_3=0;
  	delay(3);
  	LED_3=1;

   P0=seg[*(poi+1) / 16];           //第5个数码管：显示分（十位）
  	LED_2=0;
  	delay(3);
  	LED_2=1; 
  
   P0=seg[*(poi+2) % 16];             //第7个数码管：显示时（个位）
  	LED_1=0;
  	delay(3);
  	LED_1=1;
   

   P0=seg[*(poi+2) /16];             //第8个数码管：显示时（十位）
  	LED_0=0;
  	delay(3);
  	LED_0=1;/**/
}
/************************主程序**********************************************/
void main(void)
{
   unsigned char clk_time[3]={0x00,0x52,0x22};  //秒，分，时寄存器初始值
   unsigned char  temp=0x80;          
   unsigned char i;
   delay(300);
   write_1302(0x8e,0x00);             //WP=0 写操作
   for(i=0;i<3;i++)
   {
   write_1302(temp,clk_time[i]);
   temp+=2;
   }
   write_1302(0x8e,0x80);             //WP=1 写保护
   while(1)
   {  
      led_disp(clk_time);
	  temp=0x81;                    
      for(i=0;i<3;i++)
 	  {
	     clk_time[i]=read_1302(temp);
		 temp+=2;
	  }
    }
}

