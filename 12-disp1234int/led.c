#ifdef SDCC
#include <at89x52.h>
#define shuma P0
sbit at 0xB5 buz;
sbit at 0x90 led;
sbit at 0x90 LED_4;
sbit at 0x91 LED_5;
sbit at 0x92 LED_6;
sbit at 0x93 LED_7;
sbit at 0x94 LED_0;
sbit at 0x95 LED_1;
sbit at 0x96 LED_2;
sbit at 0x97 LED_3;
#else
#include <AT89X52.H>	//包含头文件
sbit buz=P3^5;
sbit led=P0^0;
#define shuma P0
sbit LED_0=P1^4;
sbit LED_1=P1^5;
sbit LED_2=P1^6;
sbit LED_3=P1^7;
sbit LED_4=P1^0;
sbit LED_5=P1^1;
sbit LED_6=P1^2;
sbit LED_7=P1^3;
#endif
unsigned char m;
unsigned char n;
//unsigned int code ton[7];
void display1(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4);
void display2(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4);
/*=====0-9=====A-G=====*/
unsigned char a[16]={0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,
             0x80,0x90,0x88,0x83,0xc6,0xa1,0x86,0x8e};
			 //共阳极数码管的段码0 1 2 3 4 5 6 7 8 9 A B C D E F

 //定时初值计算方法：以5ms为例，5ms=5000us,0xffff-5000/1.085即为TH和TL的值
void int1() interrupt 3 //T1中断，时间是5ms
{
	TR1=0; //关中断
	TH1=0xfd;//0xed; //装定时器初值实现5ms定时
	TL1=0xff;
	ET1=1; //开中断
	TR1=1;
	display1(1,2,3,4);
	display2(10,11,12,13);
	
}

void main( void )
{
	m=1;
	TMOD=0x10; //设置为T1定时器
	TH1=0xdd; //装定时器初值
	TL1=0xff;
	TR1=1; //开中断
	ET1=1;
	EA=1;
	while(1)
	{

	}
}





void display1(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4)
{
	if(m==1)
	{
		LED_0=0;		//使能该数码管控制位
		LED_1=LED_2=LED_3=1;
		LED_4=LED_5=LED_6=LED_7=1;//其他控制位无效
		shuma=a[d1];//按照数据点亮该数码管
	}
	if(m==2)
	{
		LED_1=0;
		LED_0=LED_2=LED_3=1;
		LED_4=LED_5=LED_6=LED_7=1;
		shuma=a[d2];
	}
	if(m==3)
	{
		LED_2=0;
		LED_1=LED_0=LED_3=1;
		LED_4=LED_5=LED_6=LED_7=1;
		shuma=a[d3];
	}
	if(m==4)
	{
		LED_3=0;
		LED_1=LED_2=LED_0=1;
		LED_4=LED_5=LED_6=LED_7=1;
		shuma=a[d4];
	}
	//m++;//数码管位循环扫描
	//if(m>=5)
	//	m=1;
}

void display2(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4)
{
	if(m==5)
	{
		LED_4=0;		//使能该数码管控制位
		LED_5=LED_6=LED_7=1;
		LED_0=LED_1=LED_2=LED_3=1;//其他控制位无效
		shuma=a[d1];//按照数据点亮该数码管
	}
	if(m==6)
	{
		LED_5=0;
		LED_4=LED_6=LED_7=1;
		LED_0=LED_1=LED_2=LED_3=1;
		shuma=a[d2];
	}
	if(m==7)
	{
		LED_6=0;
		LED_4=LED_5=LED_7=1;
		LED_0=LED_1=LED_2=LED_3=1;
		shuma=a[d3];
	}
	if(m==8)
	{
		LED_7=0;
		LED_4=LED_5=LED_6=1;
		LED_0=LED_1=LED_2=LED_3=1;
		shuma=a[d4];
	}
	m++;//数码管位循环扫描
	if(m>=9)
		m=1;
}
