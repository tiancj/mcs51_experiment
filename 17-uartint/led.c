#include<stdio.h>
#include<reg52.h>
#define uchar unsigned char
#define uint  unsigned int
unsigned char flag;
unsigned char s1[]="Welcome to use 51 by yoyowind.",i;
unsigned char slen=sizeof(s1);
void delay(uint x)
{
	while(x--);
}
void main()
{
	TMOD=0x20;//设置T1为工作方式2
	TH1=0xfd;//装入初值，比特率为9600bps
	TL1=0xfd;
	TR1=1;//开启T1
	REN=1;//接收允许
	SM0=0;//方式1
	SM1=1;
	EA=1;//开全局中断
	ES=1;//开串口中断
	flag=0;
	while(1)
	{
  		if(flag==1)
		{
			ES=0;
			flag=0;
			for(i=0;i<slen;i++)//若接收到，则发送相关字符串
			{
				SBUF=s1[i];
				while(!TI);
				TI=0;
			}
			ES=1;//串口中断允许
		}
	}
}
void ser() interrupt 4
{
	RI=0;//清除标志
	P2=~P2;//LED闪烁
	//P0=SBUF;
	flag=1;//发送标志
}

