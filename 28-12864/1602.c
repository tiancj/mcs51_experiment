#include<reg51.h>
#define uint unsigned int
#define uchar unsigned char
#define dat P0
sbit rs=P1^0;
sbit rw=P1^1;
sbit e=P1^2;
sbit psb=P1^3;
sbit reset=P1^5;
sbit button=P2^4;

uchar busy;             //1602判忙标志//
void delay_1ms(void);   //延时程序
void display(void);     //显示程序
void busy_1602(void);   //液晶查忙程序    
void shj_1602(uchar a);  //液晶写数据程序
void int_1602(void);     //液晶初始化
void zhl_1602(uchar a);  //液晶写指令程序


//主程序//
void main(void)
{
	int ii=0;
	psb=1;
	reset=1;

	display();
   for(;;)
   {
      

     
   }
}

//延时程序//
void delay_1ms(void)
{
  uchar i,j;
  for(i=0;i<10;i++)
  for(j=0;j<20;j++); 
}

//液晶显示程序//
void display(void)
{
       uchar i;
       uchar table0[15]={"欢迎使用ARTEKII"};  
		 uchar table1[16]={"电子产品BLUE51V4"}; 
		 uchar table2[16]={"本产品终身保修！"};
		 uchar table3[16]={"伴您成为IT高手！"};
       int_1602();
       zhl_1602(0x80);
       for(i=0;i<15;i++)
       {
            shj_1602(table0[i]);
       }
       zhl_1602(0x90);
		 for(i=0;i<16;i++)
		 {
            shj_1602(table1[i]);
		 }
		 zhl_1602(0x88);
		 for(i=0;i<16;i++)
		 {
            shj_1602(table2[i]);
		 }
		 zhl_1602(0x98);
		 for(i=0;i<16;i++)
		 {
            shj_1602(table3[i]);
		 }
} 
//查询忙碌标志信号程序//
void busy_1602(void)
{
        do
        {
            e=0;
            rw=1;
            rs=0;
            e=1;
            busy=dat;

            e=0;    
            delay_1ms();
        } while(busy&&0x80==1);
}

//写指令到LCM程序//
void zhl_1602(uchar a)
{
       busy_1602();
       e=0;
       rw=0;
       rs=0;
       e=1;
       dat=a;
       e=0;
}

//写数据到LCM程序//
void shj_1602(uchar a)
{
       busy_1602();
       e=0;
       rw=0;
       rs=1;
       e=1;
       dat=a;
       e=0;
}

//启动LCM程序//

void int_1602(void)
{
       zhl_1602(0x38); //8线控制模式
       zhl_1602(0x0e);
       zhl_1602(0x06);
}
