#include<reg51.h>
#define uint unsigned int
#define uchar unsigned char
#define dat P0
sbit rs=P1^0;
sbit rw=P1^1;
sbit e=P1^2;
uchar busy;             //1602判忙标志//
void delay_1ms(void);   //延时程序
void display(void);     //显示程序
void busy_1602(void);   //液晶查忙程序    
void shj_1602(uchar a);  //液晶写数据程序
void int_1602(void);     //液晶初始化
void zhl_1602(uchar a);  //液晶写指令程序


//主程序//
void main(void)
{
   
   for(;;)
   {
      
      display();
     
   }
}

//延时程序//
void delay_1ms(void)
{
  uchar i,j;
  for(i=0;i<10;i++)
  for(j=0;j<20;j++); 
}

//液晶显示程序//
void display(void)
{
       uchar i;
       uchar table0[15]={"Hello 51       "};  
		 uchar table1[13]={"--bluemcu   "}; 
       int_1602();
       zhl_1602(0x81);
       for(i=0;i<14;i++)
       {
            shj_1602(table0[i]);
       }
       zhl_1602(0xc4);
		 for(i=0;i<12;i++)
		 {
            shj_1602(table1[i]);
		 }
} 
//查询忙碌标志信号程序//
void busy_1602(void)
{
        do
        {
            e=0;
            rw=1;
            rs=0;
            e=1;
            busy=dat;
            e=0;    
            delay_1ms();
        } while(busy&&0x10==1);
}

//写指令到LCM程序//
void zhl_1602(uchar a)
{
       busy_1602();
       e=0;
       rw=0;
       rs=0;
       e=1;
       dat=a;
       e=0;
}

//写数据到LCM程序//
void shj_1602(uchar a)
{
       busy_1602();
       e=0;
       rw=0;
       rs=1;
       e=1;
       dat=a;
       e=0;
}

//启动LCM程序//

void int_1602(void)
{
       zhl_1602(0x38); 
       zhl_1602(0x0c);
       zhl_1602(0x06);
}
