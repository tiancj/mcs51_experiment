#include <AT89X52.H>	//包含头文件

#define shuma P0
sbit LED_0=P1^0;
sbit LED_1=P1^1;
sbit LED_2=P1^2;
sbit LED_3=P1^3;
sbit add=P2^6;	//加
sbit dec=P2^7;	//减
sbit buz=P3^3;
//#define led P2


unsigned char m,n;
unsigned int d;
unsigned int code ton[7];
void display(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4);
/*=====0-9=====A-G=====*/
unsigned char a[16]={0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,
             0x80,0x90,0x88,0x83,0xc6,0x86,0x8e,0x82};
			 //共阳极数码管的段码0 1 2 3 4 5 6 7 8 9 A B C D E F

 //定时初值计算方法：以5ms为例，5ms=5000us,0xffff-5000/1.085即为TH和TL的值
void int1() interrupt 3 //T1中断，时间是5ms
{
	unsigned char d1,d2,d3,d4;
	TR1=0; //关中断
	TH1=0xed; //装定时器初值实现5ms定时
	TL1=0xff;
	ET1=1; //开中断
	TR1=1;
	d4=(d%60)%10;
	d3=(d%60)/10;
	d2=(d/60)%10;
	d1=(d/60)/10;
	display(d1,d2,d3,d4);

		
	n++;
	if(n>=200)
	{
		n=0;
		if(d)
			d--;
		if(d==0)
		{
			buz=~buz;
			//led=~led;
		}
	}
	
}

void main( void )
{
	unsigned int i;
	d=10;//倒计时10秒
	m=1;
	TMOD=0x10; //设置为T1定时器
	TH1=0xed; //装定时器初值
	TL1=0xff;
	TR1=1; //开中断
	ET1=1;
	EA=1;/**/
	while(1)
	{
		if(add==0 || dec==0)
		{
			/*P2=P3;
			i=10000;
			while(i--);
			if(add==0)
				d++;
			if(dec==0)
				d--;*/
		}
	}
}





void display(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4)
{
	if(m==1)
	{
		LED_0=0;		//使能该数码管控制位
		LED_1=LED_2=LED_3=1;//其他控制位无效
		shuma=a[d1];//按照数据点亮该数码管
	}
	if(m==2)
	{
		LED_1=0;
		LED_0=LED_2=LED_3=1;
		shuma=a[d2];
	}
	if(m==3)
	{
		LED_2=0;
		LED_1=LED_0=LED_3=1;
		shuma=a[d3];
	}
	if(m==4)
	{
		LED_3=0;
		LED_1=LED_2=LED_0=1;
		shuma=a[d4];
	}
	m++;//数码管位循环扫描
	if(m>=5)
		m=1;
}


