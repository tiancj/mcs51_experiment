#include <AT89X52.H>	//包含头文件

unsigned char date;
unsigned char recFlag;//接收数据标识，0 未接收数据 1 接收数据

void init_serial();
void send();
void receive();

main()
{
	init_serial();
	IE=0; //屏蔽中断
    while(1)
	{
    	receive();
    	send();
	}
}
void init_serial()    //初始化串口
{
	TMOD=0x20;    //定时器T1使用工作方式2
	TH1=0xfd;     //设置初值
	TH0=0xfd;
	TR1=1;      //开始计时
	PCON=0x00;     //SMOD=0；
	SCON=0x50;     //工作方式1，波特率9600bit/s,允许接收
	TI=1;
	EA=1;
}
void send()
{
	if(TI==1)     //检测输出是否READY
	{
		if(recFlag==1)     //是否接收过数据
		{
			SBUF=date;          //发送数据
			recFlag=0;
			TI=0;
		}
	}
}

void receive()
{
	if(RI==1)    //检测是否有数据接收
	{
		date=SBUF;      //接收数据
		recFlag=1;             //设置接收标识符
		RI=0;	//清除标志位
	}
}


