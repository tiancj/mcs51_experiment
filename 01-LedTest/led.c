#ifdef SDCC
#include <at89x52.h>	//包含头文件
#else
#include <AT89X52.H>	//包含头文件
#endif

void main(void)		//主函数
{
	unsigned int i;
	P0=0XFF;	//P2口各脚输出高电平，LED灭

	while(1)
	{
		for(i = 0; i < 30000; i++) ;//延时
		P0 = ~P0;			//P2取反，实现灯亮灭闪烁
	}
}
