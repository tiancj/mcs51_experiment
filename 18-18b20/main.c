#pragma db code
#include<AT89X52.H>
//#include "reg52.h"
#include "INTRINS.H"
// 此实验是使用18B20检测温度，然后在数码管上显示

#define uchar unsigned char
#define uint  unsigned int
#define   BUSY1    (DQ1==0) 
sbit LED_0=P1^0;
sbit LED_1=P1^1;
sbit LED_2=P1^2;
sbit LED_3=P1^3;
sbit DQ1=P3^6;


//void delay(uint x);
void display(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4);
void ds_reset_1(void);
void wr_ds18_1(char dat);
void time_delay(unsigned char time);
int get_temp_1(void);
void delay(unsigned int x);
void read_ROM(void);
int get_temp_d(void);

/*=====0-9=====A-G=====*/
uchar a[16]={0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,
             0x80,0x90,0x88,0x83,0xc6,0x86,0x8e,0x82};
unsigned char ResultSignal;
int ResultTemperatureLH,ResultTemperatureLL,ResultTemperatureH;
unsigned char ROM[8];
unsigned char idata TMP; 
unsigned char idata TMP_d; 
unsigned char f; 
unsigned char rd_ds18_1();
unsigned int TemH,TemL;



void main()
{
  	unsigned int TemH,TemL,k=0,count;

	ds_reset_1();
	ds_reset_1();	//reset
	wr_ds18_1(0xcc);	//skip rom
	_nop_();
			wr_ds18_1(0x7f);
		ds_reset_1();
 

		wr_ds18_1(0xcc);
				_nop_();  
		wr_ds18_1(0x44);

		for(k=0;k<11000;k++)
			time_delay(255);   	
	ds_reset_1();


   	while(1)
  	{
		wr_ds18_1(0xcc);
		wr_ds18_1(0xbe);
		TemH=get_temp_1();
		TemL=get_temp_d();

		TemH&=0x00ff;
		TemL&=0x00ff;
		count=(TemH*256+TemL)*6.25;
	  	display((TemH/10)%10,TemH%10,((count/10)%10),(count%10));
  	}
}

/***************延时程序，单位us,大于10us*************/
void time_delay(unsigned char time)
{
 
 	time=time-10;
 	time=time/6;
 	while(time!=0)time--;
}


/*****************************************************/
/*                reset ds18b20                      */
/*****************************************************/
void ds_reset_1(void)
{
 	unsigned char idata count=0;  
 	DQ1=0;
  	time_delay(240);
	time_delay(240);
 	DQ1=1;
 	return;
}

  

void check_pre_1(void)
{
    	while(DQ1);
    	while(~DQ1);
   
         time_delay(30);
}


void read_ROM(void)
{
	int n;
	ds_reset_1();
 	check_pre_1();
 	wr_ds18_1(0x33);
 	for(n=0;n<8;n++){ROM[n]=rd_ds18_1();}   
} 


/*****************************************************/
/*      Read a bit from 1820      位读取             */
/*****************************************************/
bit tmrbit_1(void)
{
        idata char i=0;
    	bit dat;
    	
    	DQ1=0;_nop_();
  	
        DQ1=1;
        
  	_nop_();
  	_nop_();
  	_nop_();
	_nop_();
 	_nop_();
  	_nop_();
  	_nop_();
  	_nop_();
        _nop_();
 	_nop_();
  	_nop_();
  	_nop_();
  	_nop_();
  	_nop_(); 
  
  	
    	dat = DQ1;
    	
        time_delay(50);
      
    	return dat;
}

  


/*****************************************************/
/*                read a bety from ds18b20 字节读取  */
/*****************************************************/
unsigned char rd_ds18_1()
{
 	unsigned char idata i,j,dat=0;
    	for(i=1;i<=8;i++)
    	{
        	j=tmrbit_1();
        	dat=(j<<(i-1))|dat;
    	}
    	return dat;
}

 


/*****************************************************/
/*         write a bety from ds18b20   写字节        */
/****************************************************/
void wr_ds18_1(char dat)
{
    	signed char  idata i=0;
    	unsigned char idata j;
    	bit testb;

    	for(j=1;j<=8;j++)
    	{
        	testb=dat & 0x01;
        	dat = dat>>1;
        	if(testb)
        	{
            		DQ1=0;
             		_nop_();
    			_nop_();
            		DQ1=1;
          		time_delay(60);   
           	}
        	
        	else
        	{
            		DQ1=0;
            		time_delay(50);
                   
            		DQ1=1;
             		_nop_();
    			_nop_();
        	}
    	}
}

 

int get_temp_1(void)
{
 	unsigned char idata a=0,b=0; 
 	unsigned char idata i;
 	EA=0; 
 	
 	ds_reset_1();
 	check_pre_1();
 	
 	wr_ds18_1(0xcc);                   
 	wr_ds18_1(0x44);      	
 	while(BUSY1);
 	
 	ds_reset_1();
 	check_pre_1();
 	wr_ds18_1(0xcc);                 
 	wr_ds18_1(0xbe);                  
 	
 	a=rd_ds18_1();
 	b=rd_ds18_1();
 	
 	i=b;   /*若b为1则为负温   */
 	i=(i>>4);
 	if(i==0)
 	{
 		f=0;
 		TMP=((a>>4)|(b<<4));
		a=(a&0x0f);
		if (a>8)
		{
			TMP=(TMP+1);
		}
 	}
 	else 
 	{
 		f=1;
		a=a>>4; 
 		b=b<<4;
 		 		
 		TMP=(a|b);
 		 
 		TMP=~TMP;
 		TMP=(TMP+1);
 	}
 	EA=1;
 	return(TMP);
}

   
 

int get_temp_d(void)
{
 	unsigned char idata a=0,b=0; 
 	unsigned char idata i,m;
 	EA=0; 
 	
 	ds_reset_1();//复位
 	check_pre_1();
 	
 	wr_ds18_1(0xcc);        
 	wr_ds18_1(0x44);      	
 	while(BUSY1);
 	
 	ds_reset_1();
 	check_pre_1();
 	wr_ds18_1(0xcc);        
 	wr_ds18_1(0xbe);        
 	
 	a=rd_ds18_1();
 	b=rd_ds18_1();
 	
 	
 	i=b;   /*若b为1则为负温   */
 	i=(i>>4);
 	 
 	if(i==0)
 	{
 		f=0;
 		TMP=((a>>4)|(b<<4));
		a=(a&0x0f);
		TMP_d=a;
 	}
 	else 
 	{
 		f=1;
 		a=~a;
 		a=(a+1);
 		b=~b;
 		b=(b+1);
 		
 		m=a;
 		a=a>>4; 
 		b=b<<4; 
 		 		
 		TMP=(a|b); 		 
 		m=(m&0x0f);
 		TMP_d=m;
 	}
 	
 	EA=1;
 	return(TMP_d);
}void delay(unsigned int x)
{
  	unsigned int i;
  	for(i=0;i<x;i++);
}
void display(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4)
{
  	P0=a[d1];
  	LED_0=0;
  	delay(100);
  	LED_0=1;

  	P0=a[d2];
  	LED_1=0;
  	delay(100);
  	LED_1=1;

  	P0=a[d3];
  	LED_2=0;
  	delay(100);
  	LED_2=1;

  	P0=a[d4];
  	LED_3=0;
  	delay(100);
  	LED_3=1; 
}
