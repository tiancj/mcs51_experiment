#pragma db code
#include<AT89X52.H>
//#include "reg52.h"
#include "INTRINS.H"
// 此实验是使用18B20检测温度，然后在数码管上显示

#define uchar unsigned char
#define uint  unsigned int
#define   BUSY1    (DQ1==0) 
sbit LED_0=P1^0;
sbit LED_1=P1^1;
sbit LED_2=P1^2;
sbit LED_3=P1^3;
sbit LED_4=P1^4;
sbit LED_5=P1^5;
sbit LED_6=P1^6;
sbit LED_7=P1^7;
sbit DQ1=P3^6;
sbit buz=P3^3;
sbit jdq=P3^2;
sbit but1=P2^4;
sbit but2=P2^5;
sbit but3=P2^6;
sbit but4=P2^7;

unsigned char date;
unsigned char recFlag;//接收数据标识，0 未接收数据 1 接收数据

void init_serial();
void send();
void receive();
//void delay(uint x);
void display(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4);
void display2(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4);
void ds_reset_1(void);
void wr_ds18_1(char dat);
void time_delay(unsigned char time);
int get_temp_1(void);
void delay(unsigned int x);
void read_ROM(void);
int get_temp_d(void);

/*=====0-9=====A-G=====*/
uchar a[22]={0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,
             0x80,0x90,0x88,0x83,0xc6,0x86,0x8e,0x82,/*b 16*/0x83,/*L*/0xc7,/*U*/0xc1,/*E*/0x86,/*v*/0xe3,/*null*/0xff};
unsigned char ResultSignal;
int ResultTemperatureLH,ResultTemperatureLL,ResultTemperatureH;
unsigned char ROM[8];
unsigned char idata TMP; 
unsigned char idata TMP_d; 
unsigned char f; 
unsigned char rd_ds18_1();
unsigned int TemH,TemL;
unsigned char mode;



void main()
{
  	unsigned int TemH,TemL,k=0,count;
	unsigned char disline,a,b,c,d;
	a=b=c=d=0;
	disline=1;
	date=124;
	init_serial();
	ds_reset_1();
	ds_reset_1();	//reset
	wr_ds18_1(0xcc);	//skip rom
	_nop_();
			wr_ds18_1(0x7f);
		ds_reset_1();
 

		wr_ds18_1(0xcc);
				_nop_();  
		wr_ds18_1(0x44);

		for(k=0;k<11000;k++)
			time_delay(255);   	
	ds_reset_1();


   	while(1)
  	{
		receive();
		if(date==33)
			buz=0;
		if(date==35)
			buz=1;
		if(date==36)
			jdq=0;
		if(date==37)
			jdq=1;
		if(date==38 && mode==0)
			P0=0xfe;
		if(date==39 && mode==0)
			P0=0xfd;
		if(date==40 && mode==0)
			P0=0xfb;
		if(date==41 && mode==0)
			P0=0xf7;
		if(date==42 && mode==0)
			P0=0xef;
		if(date==43 && mode==0)
			P0=0xdf;
		if(date==44 && mode==0)
			P0=0xbf;
		if(date==45 && mode==0)
			P0=0x7f;
		if(date>=50 && date<=59)
		{	
			date-=50;
			display2(date,date,date,date);
			date+=50;
		}
		if(but1==0)
		{
			if(TI==1)     //检测输出是否READY
			{

				SBUF=71;          //发送数据
				recFlag=0;
				TI=0;
			}
		}
		if(but2==0)
		{
			if(TI==1)     //检测输出是否READY
			{

				SBUF=72;          //发送数据
				recFlag=0;
				TI=0;
			}
		}
		if(but3==0)
		{
			if(TI==1)     //检测输出是否READY
			{

				SBUF=73;          //发送数据
				recFlag=0;
				TI=0;
			}
		}
		if(but4==0)
		{
			if(TI==1)     //检测输出是否READY
			{

				SBUF=74;          //发送数据
				recFlag=0;
				TI=0;
			}
		}

		if(date==124)
		{
			display2(16,17,18,19);
			display(5,1,20,4);
			mode=0;
		}
		if(date==125 || date==33 || date==35 || date==36 || date==37)
		{
			mode=1;
			wr_ds18_1(0xcc);
			wr_ds18_1(0xbe);
			TemH=get_temp_1();
			TemL=get_temp_d();

			TemH&=0x00ff;
			TemL&=0x00ff;
			count=(TemH*256+TemL)*6.25;
	  		display((TemH/10)%10,TemH%10,((count/10)%10),(count%10));
		//串口发送	
			if(TI==1)     //检测输出是否READY
			{

				SBUF=41+TemH;          //发送数据
				recFlag=0;
				TI=0;
			}
		}
  	}
}

/***************延时程序，单位us,大于10us*************/
void time_delay(unsigned char time)
{
 
 	time=time-10;
 	time=time/6;
 	while(time!=0)time--;
}


/*****************************************************/
/*                reset ds18b20                      */
/*****************************************************/
void ds_reset_1(void)
{
 	unsigned char idata count=0;  
 	DQ1=0;
  	time_delay(240);
	time_delay(240);
 	DQ1=1;
 	return;
}

  

void check_pre_1(void)
{
    	while(DQ1);
    	while(~DQ1);
   
         time_delay(30);
}


void read_ROM(void)
{
	int n;
	ds_reset_1();
 	check_pre_1();
 	wr_ds18_1(0x33);
 	for(n=0;n<8;n++){ROM[n]=rd_ds18_1();}   
} 


/*****************************************************/
/*      Read a bit from 1820      位读取             */
/*****************************************************/
bit tmrbit_1(void)
{
        idata char i=0;
    	bit dat;
    	
    	DQ1=0;_nop_();
  	
        DQ1=1;
        
  	_nop_();
  	_nop_();
  	_nop_();
	_nop_();
 	_nop_();
  	_nop_();
  	_nop_();
  	_nop_();
        _nop_();
 	_nop_();
  	_nop_();
  	_nop_();
  	_nop_();
  	_nop_(); 
  
  	
    	dat = DQ1;
    	
        time_delay(50);
      
    	return dat;
}

  


/*****************************************************/
/*                read a bety from ds18b20 字节读取  */
/*****************************************************/
unsigned char rd_ds18_1()
{
 	unsigned char idata i,j,dat=0;
    	for(i=1;i<=8;i++)
    	{
        	j=tmrbit_1();
        	dat=(j<<(i-1))|dat;
    	}
    	return dat;
}

 


/*****************************************************/
/*         write a bety from ds18b20   写字节        */
/****************************************************/
void wr_ds18_1(char dat)
{
    	signed char  idata i=0;
    	unsigned char idata j;
    	bit testb;

    	for(j=1;j<=8;j++)
    	{
        	testb=dat & 0x01;
        	dat = dat>>1;
        	if(testb)
        	{
            		DQ1=0;
             		_nop_();
    			_nop_();
            		DQ1=1;
          		time_delay(60);   
           	}
        	
        	else
        	{
            		DQ1=0;
            		time_delay(50);
                   
            		DQ1=1;
             		_nop_();
    			_nop_();
        	}
    	}
}

 

int get_temp_1(void)
{
 	unsigned char idata a=0,b=0; 
 	unsigned char idata i;
 	EA=0; 
 	
 	ds_reset_1();
 	check_pre_1();
 	
 	wr_ds18_1(0xcc);                   
 	wr_ds18_1(0x44);      	
 	while(BUSY1);
 	
 	ds_reset_1();
 	check_pre_1();
 	wr_ds18_1(0xcc);                 
 	wr_ds18_1(0xbe);                  
 	
 	a=rd_ds18_1();
 	b=rd_ds18_1();
 	
 	i=b;   /*若b为1则为负温   */
 	i=(i>>4);
 	if(i==0)
 	{
 		f=0;
 		TMP=((a>>4)|(b<<4));
		a=(a&0x0f);
		if (a>8)
		{
			TMP=(TMP+1);
		}
 	}
 	else 
 	{
 		f=1;
		a=a>>4; 
 		b=b<<4;
 		 		
 		TMP=(a|b);
 		 
 		TMP=~TMP;
 		TMP=(TMP+1);
 	}
 	EA=1;
 	return(TMP);
}

   
 

int get_temp_d(void)
{
 	unsigned char idata a=0,b=0; 
 	unsigned char idata i,m;
 	EA=0; 
 	
 	ds_reset_1();//复位
 	check_pre_1();
 	
 	wr_ds18_1(0xcc);        
 	wr_ds18_1(0x44);      	
 	while(BUSY1);
 	
 	ds_reset_1();
 	check_pre_1();
 	wr_ds18_1(0xcc);        
 	wr_ds18_1(0xbe);        
 	
 	a=rd_ds18_1();
 	b=rd_ds18_1();
 	
 	
 	i=b;   /*若b为1则为负温   */
 	i=(i>>4);
 	 
 	if(i==0)
 	{
 		f=0;
 		TMP=((a>>4)|(b<<4));
		a=(a&0x0f);
		TMP_d=a;
 	}
 	else 
 	{
 		f=1;
 		a=~a;
 		a=(a+1);
 		b=~b;
 		b=(b+1);
 		
 		m=a;
 		a=a>>4; 
 		b=b<<4; 
 		 		
 		TMP=(a|b); 		 
 		m=(m&0x0f);
 		TMP_d=m;
 	}
 	
 	EA=1;
 	return(TMP_d);
}void delay(unsigned int x)
{
  	unsigned int i;
  	for(i=0;i<x;i++);
}
void display(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4)
{
  	P0=a[d1];
  	LED_0=0;
  	delay(100);
  	LED_0=1;

  	P0=a[d2];
  	LED_1=0;
  	delay(100);
  	LED_1=1;

  	P0=a[d3];
  	LED_2=0;
  	delay(100);
  	LED_2=1;

  	P0=a[d4];
  	LED_3=0;
  	delay(100);
  	LED_3=1; 
}
void display2(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4)
{
  	P0=a[d1];
  	LED_4=0;
  	delay(100);
  	LED_4=1;

  	P0=a[d2];
  	LED_5=0;
  	delay(100);
  	LED_5=1;

  	P0=a[d3];
  	LED_6=0;
  	delay(100);
  	LED_6=1;

  	P0=a[d4];
  	LED_7=0;
  	delay(100);
  	LED_7=1; 
}
void init_serial()    //初始化串口
{
	TMOD=0x20;    //定时器T1使用工作方式2
	TH1=0xfd;     //设置初值
	TH0=0xfd;
	TR1=1;      //开始计时
	PCON=0x00;     //SMOD=0；
	SCON=0x50;     //工作方式1，波特率9600bit/s,允许接收
	TI=1;
	EA=1;
}

void receive()
{
	if(RI==1)    //检测是否有数据接收
	{
		date=SBUF;      //接收数据
		RI=0;	//清除标志位
	}
}

