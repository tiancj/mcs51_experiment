#include <AT89X52.H>	//包含头文件
//#pragma db code
#include<AT89X52.H>
//#include "reg52.h"
//#include "INTRINS.H"


#define shuma P0
sbit LED_0=P1^4;
sbit LED_1=P1^5;
sbit LED_2=P1^6;
sbit LED_3=P1^7;
sbit LED_4=P1^0;
sbit LED_5=P1^1;
sbit LED_6=P1^2;
sbit LED_7=P1^3;




void delay(unsigned int x);
void display1(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4);
void display2(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4);


/*=====0-9=====A-G=====*/
unsigned char a[16]={0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,
             0x80,0x90,0x88,0x83,0xc6,0xa1,0x86,0x8e};
			 //共阳极数码管的段码0 1 2 3 4 5 6 7 8 9 A B C D E F




void main()
{
   	while(1)
  	{
	  	display1(1,2,3,4);
		display2(10,11,12,13);
  	}
}



void delay(unsigned int x)
{
  	unsigned int i;
  	for(i=0;i<x;i++);
}
void display1(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4)
{
  	shuma=a[d1];	//选中第一位，发送第一位段码
  	LED_0=0;
  	delay(100);
  	LED_0=1;

  	shuma=a[d2];//选中第二位，发送第二位段码
  	LED_1=0;
  	delay(100);
  	LED_1=1;

  	shuma=a[d3];//选中第三位，发送第三位段码
  	LED_2=0;
  	delay(100);
  	LED_2=1;

  	shuma=a[d4];//选中第四位，发送第四位段码
  	LED_3=0;
  	delay(100);
  	LED_3=1; 


}
void display2(unsigned char d1,unsigned char d2,unsigned char d3,unsigned char d4)
{
  	shuma=a[d1];	//选中第五位，发送第一位段码
  	LED_4=0;
  	delay(100);
  	LED_4=1;

  	shuma=a[d2];//选中第六位，发送第二位段码
  	LED_5=0;
  	delay(100);
  	LED_5=1;

  	shuma=a[d3];//选中第七位，发送第三位段码
  	LED_6=0;
  	delay(100);
  	LED_6=1;

  	shuma=a[d4];//选中第八位，发送第四位段码
  	LED_7=0;
  	delay(100);
  	LED_7=1; 
}
